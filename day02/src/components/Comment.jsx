import React, { Component } from 'react'
import '../css/index.css'

import classNames from 'classnames'

export default class Comment extends Component {
  // 定义状态
  state = {
    // 当前用户
    user: {
      name: '清风徐来',
      vip: true,
      avatar: 'https://static.youku.com/lvip/img/avatar/310/6.png'
    },
    // 评论列表
    comments: [
      {
        id: 100,
        name: '__RichMan',
        avatar: 'https://r1.ykimg.com/051000005BB36AF28B6EE4050F0E3BA6',
        content:
          '这阵容我喜欢😍靳东&闫妮，就这俩名字，我就知道是良心剧集...锁了🔒',
        time: '2021/10/12 10:10:23',
        vip: true,
        collect: false
      },
      {
        id: 101,
        name: '糖蜜甜筒颖',
        avatar:
          'https://image.9xsecndns.cn/image/uicon/712b2bbec5b58d6066aff202c9402abc3370674052733b.jpg',
        content:
          '突围神仙阵容 人民的名义第三部来了 靳东陈晓闫妮秦岚等众多优秀演员实力派 守护人民的财产 再现国家企业发展历程',
        time: '2021/09/23 15:12:44',
        vip: false,
        collect: true
      },
      {
        id: 102,
        name: '清风徐来',
        avatar: 'https://static.youku.com/lvip/img/avatar/310/6.png',
        content:
          '第一集看的有点费力，投入不了，闫妮不太适合啊，职场的人哪有那么多表情，一点职场的感觉都没有',
        time: '2021/07/01 00:30:51',
        vip: true,
        collect: false
      }
    ],
    content: ''
  }

  // 删除事件
  delItem = id => {
    const data = this.state.comments.filter(item => item.id !== id)
    this.setState({
      comments: data
    })
  }
  // 绑定输入框
  handleContent = e => {
    const { value } = e.target
    if (value.length > 100) return
    this.setState({
      content: value.trim()
    })
  }
  // 点击发布评论
  submit = () => {
    // 判断评论是否重复
    const flag = this.state.comments.some(
      item => item.content === this.state.content
    )
    if (flag) {
      this.setState({
        content: ''
      })
      return alert('注意：有相同评论！！')
    }
    if (this.state.content) {
      const item = {
        id: Math.random(),
        ...this.state.user,
        content: this.state.content,
        time: new Date().toLocaleString('chinese', { hour12: false }),
        collect: false
      }
      // 追加列表，清空输入
      this.setState({
        comments: [item, ...this.state.comments],
        content: ''
      })
    }
  }
  // 收藏
  collectComment = id => {
    const { comments } = this.state
    this.setState({
      comments: comments.map(item => {
        if (item.id === id) {
          return { ...item, collect: !item.collect }
        } else {
          return item
        }
      })
    })
  }

  render() {
    const { comments, user, content } = this.state
    return (
      <div className="comments">
        <h3 className="comm-head">评论</h3>
        <div className="comm-input">
          <textarea
            placeholder="爱发评论的人，运气都很棒"
            value={content}
            onChange={this.handleContent}
          ></textarea>
          <div className="foot">
            <div className="word">{content.length}/100</div>
            <div className="btn" onClick={this.submit}>
              发表评论
            </div>
          </div>
        </div>
        <h3 className="comm-head">
          热门评论<sub>({comments.length})</sub>
        </h3>
        <ul className="comm-list">
          {comments.map(item => {
            return (
              <li className="comm-item" key={item.id}>
                <img className="avatar" src={item.avatar}></img>
                <div className="info">
                  {/* <p className={['name', item.vip ? 'vip' : ''].join(' ')}> */}
                  <p className={classNames('name', { vip: item.vip })}>
                    {item.name}
                    <img
                      alt=""
                      src={
                        item.vip
                          ? 'https://gw.alicdn.com/tfs/TB1c5JFbGSs3KVjSZPiXXcsiVXa-48-48.png'
                          : ''
                      }
                    />
                  </p>
                  <p className="time">
                    {item.time}
                    <span
                      className={`iconfont icon-collect${
                        item.collect ? '-sel' : ''
                      }`}
                      onClick={() => this.collectComment(item.id)}
                    ></span>
                    {user.name === item.name ? (
                      <span
                        className="del"
                        onClick={() => this.delItem(item.id)}
                      >
                        删除
                      </span>
                    ) : null}
                  </p>
                  <p>{item.content}</p>
                </div>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}
