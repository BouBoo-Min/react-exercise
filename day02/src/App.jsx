import { Component } from 'react'

import Comment from './components/Comment.jsx'

class App extends Component {
  render() {
    return (
      <>
        <Comment />
      </>
    )
  }
}

export default App
