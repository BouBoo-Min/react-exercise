const Header = ({ addTodo }) => {
  // 添加todo
  const setTodo = (e) => {
    // console.log(e.target.value)
    // console.log(e.keyCode)
    const { target, keyCode } = e
    if (keyCode !== 13) return
    if (target.value.trim() === '') alert('输入框不能为空！')
    const obj = { id: Math.random() * 100, name: target.value, done: false }
    addTodo(obj)
    target.value = ''
  }
  return (
    <header className="header">
      <h1>todos</h1>
      <input
        className="new-todo"
        placeholder="What needs to be done?"
        autoFocus
        onKeyUp={setTodo}
      />
    </header>
  )
}

export default Header
