import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    const { list, active, delAllDone, changeActive } = this.props
    const noDone = list.filter((item) => item.done === false)
    const noTotal = noDone.length
    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{noTotal}</strong> item left
        </span>
        <ul className="filters">
          <li>
            <a
              href="#/"
              className={active === 'All' ? 'selected' : ''}
              onClick={() => changeActive('All')}
            >
              All
            </a>
          </li>
          <li>
            <a
              href="#/active"
              className={active === 'Active' ? 'selected' : ''}
              onClick={() => changeActive('Active')}
            >
              Active
            </a>
          </li>
          <li>
            <a
              href="#/completed"
              className={active === 'Completed' ? 'selected' : ''}
              onClick={() => changeActive('Completed')}
            >
              Completed
            </a>
          </li>
        </ul>
        <button className="clear-completed" onClick={() => delAllDone()}>
          Clear completed
        </button>
      </footer>
    )
  }
}
