const Main = ({ list, delTodo, changeCheck, allChecked, active }) => {
  const total = list.length
  const count = list.reduce((sum, item) => {
    return (sum += item.done ? 1 : 0)
  }, 0)
  let renderList = null
  if (active === 'All') {
    renderList = list
  } else if (active === 'Active') {
    renderList = list.filter((item) => item.done === false)
  } else {
    renderList = list.filter((item) => item.done === true)
  }
  return (
    <section className="main">
      <input
        id="toggle-all"
        className="toggle-all"
        type="checkbox"
        checked={total === count && total !== 0 ? true : false}
        onChange={(e) => allChecked(e.target.checked)}
      />
      <label htmlFor="toggle-all">Mark all as complete</label>
      <ul className="todo-list">
        {renderList.map((item) => (
          <li className={item.done ? 'completed' : ''} key={item.id}>
            <div className="view">
              <input
                className="toggle"
                type="checkbox"
                checked={item.done}
                onChange={() => changeCheck(item.id)}
              />
              <label>{item.name}</label>
              <button
                className="destroy"
                onClick={() => delTodo(item.id)}
              ></button>
            </div>
            <input className="edit" defaultValue="Create a TodoMVC template" />
          </li>
        ))}
      </ul>
    </section>
  )
}

export default Main
